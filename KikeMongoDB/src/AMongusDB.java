import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.bson.Document;

import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.awt.event.ActionEvent;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Indexes;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.DeleteResult;

import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class AMongusDB extends JFrame {

	private JPanel contentPane;
	private JTextField ConnectDatabaseText;
	private DB db;
	private DBCollection coll;
	MongoCollection<Document> coll1;
	MongoDatabase database;
	String databasetext;
	private JTextField ShowCollectionText;
	private JButton DeleteDatabaseButton;
	private JButton CreateCollectionButton;
	private JButton DeleteCollectionButton;
	private JTextField Value1Text;
	private JTextField Value2Text;
	private JTextField Value3Text;
	BasicDBObject document = new BasicDBObject();
	BasicDBObject query = new BasicDBObject();
	BasicDBObject newDocument= new BasicDBObject();
	BasicDBObject updateObject=new BasicDBObject();
	BasicDBObject field = new BasicDBObject();
	DeleteResult result;
	private JButton UpdateCollectionButton;
	private JTextField UpdateField1Text;
	private JTextField UpdateValue1Text;
	private JTextField UpdateValue2Text;
	private JTextField field1Text;
	private JTextField field2Text;
	private JTextField field3Text;
	private JTextField ShowField1Text;
	private JButton ShowValueButton;
	private JTextField ShowValue1Text;
	private JTextField ShowField2Text;
	private JTextField ShowValue2Text;
	private JButton $inButton;
	private JButton $gtButton;
	private JButton $ltButton;
	private JButton $gt$ltButton;
	private JButton $neButton;
	private JButton $andButton;
	private JTextField UpdateField2Text;
	private JButton AddToDocButton;
	private JButton CreateIndexButton;
	private JTextField CreateIndexText;
	private JButton ShowIndexesButton;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		AMongusDB connect = new AMongusDB();
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AMongusDB frame = new AMongusDB();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AMongusDB() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 545);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		ConnectDatabaseText = new JTextField();
		ConnectDatabaseText.setBounds(10, 48, 200, 23);
		contentPane.add(ConnectDatabaseText);
		ConnectDatabaseText.setColumns(10);
		JButton ConnectDatabaseButton =new JButton("ConnectDatabase");
		ConnectDatabaseButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MongoClient mongoClient = new MongoClient("localhost",27017);
				db = mongoClient.getDB(ConnectDatabaseText.getText());
				database= mongoClient.getDatabase(ConnectDatabaseText.getText());
				System.out.println("Connected to database");
				
				
				for (String collNames : db.getCollectionNames()) {
				    System.out.println(collNames);
				}

			}
		});
		ConnectDatabaseButton.setBounds(10, 0, 200, 23);
		contentPane.add(ConnectDatabaseButton);

		
		
		ShowCollectionText = new JTextField();
		ShowCollectionText.setBounds(10, 151, 200, 20);
		contentPane.add(ShowCollectionText);
		ShowCollectionText.setColumns(10);

		
		JButton ShowCollection = new JButton("ShowCollection");
		ShowCollection.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					System.out.println("Estos son los datos de tu collection");
					coll = db.getCollection(ShowCollectionText.getText());
					DBCursor cursor = coll.find();
					coll1= database.getCollection(ShowCollectionText.getText());
					while(cursor.hasNext()) {
							System.out.println(cursor.next());
					}
			}
		});
		ShowCollection.setBounds(10, 117, 200, 23);
		contentPane.add(ShowCollection);
		
		DeleteDatabaseButton = new JButton("DeleteDatabase");
		DeleteDatabaseButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				db.dropDatabase();
				System.out.println("Has borrado la base de datos "+ConnectDatabaseText.getText());
			}
		});
		DeleteDatabaseButton.setBounds(233, 0, 191, 23);
		contentPane.add(DeleteDatabaseButton);
		
		CreateCollectionButton = new JButton("CreateCollection");
		CreateCollectionButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				db.createCollection(ShowCollectionText.getText(),new BasicDBObject()); 
				System.out.println(ShowCollectionText.getText()+" ha sido creada");
			}
		});
		CreateCollectionButton.setBounds(233, 117, 191, 23);
		contentPane.add(CreateCollectionButton);
		
		JButton CreateDatabaseButton = new JButton("CreateDatabase");
		CreateDatabaseButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MongoClient mongoClient2 = new MongoClient("localhost",27017);
				mongoClient2.getDatabase(ConnectDatabaseText.getText()).createCollection(ShowCollectionText.getText());
				System.out.println("Has creado la base de datos "+ConnectDatabaseText.getText());
			}
		});
		CreateDatabaseButton.setBounds(233, 48, 191, 23);
		contentPane.add(CreateDatabaseButton);
		
		DeleteCollectionButton = new JButton("DeleteCollection");
		DeleteCollectionButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				db.getCollection(ShowCollectionText.getText()).drop(); 
				System.out.println(ShowCollectionText.getText()+" ha sido borrada");
			}
		});
		DeleteCollectionButton.setBounds(233, 150, 191, 23);
		contentPane.add(DeleteCollectionButton);
		
		JLabel CollectionDataLabel = new JLabel("                      CollectionData");
		CollectionDataLabel.setToolTipText("");
		CollectionDataLabel.setBounds(10, 195, 200, 14);
		contentPane.add(CollectionDataLabel);
		
		Value1Text = new JTextField();
		Value1Text.setBounds(92, 234, 139, 20);
		contentPane.add(Value1Text);
		Value1Text.setColumns(10);
		
		Value2Text = new JTextField();
		Value2Text.setBounds(92, 254, 139, 20);
		contentPane.add(Value2Text);
		Value2Text.setColumns(10);
		
		Value3Text = new JTextField();
		Value3Text.setBounds(92, 273, 139, 20);
		contentPane.add(Value3Text);
		Value3Text.setColumns(10);
		
		
		JButton AddDataButton = new JButton("AddData");
		AddDataButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {		
			    document.put(field1Text.getText(),Value1Text.getText());
			    document.put(field2Text.getText(), Value2Text.getText());
			    document.put(field3Text.getText(), Value3Text.getText());
			    coll.insert(document);
			    System.out.println("Has insertado este documento ");
			    System.out.println(document);
			    document.clear();
			}
		});
		AddDataButton.setBounds(285, 233, 139, 23);
		contentPane.add(AddDataButton);
		
		UpdateCollectionButton = new JButton("UpdateCollection");
		UpdateCollectionButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				query.put(UpdateField1Text.getText(),UpdateValue1Text.getText());
				newDocument.put(UpdateField1Text.getText(), UpdateValue2Text.getText());
				updateObject.put("$set",newDocument);
				coll.update(query, updateObject);
				System.out.println("Has actualizado tu collection");
				query.clear();
				newDocument.clear();
				updateObject.clear();
				}
		});
		UpdateCollectionButton.setBounds(285, 253, 139, 23);
		contentPane.add(UpdateCollectionButton);
		
		UpdateField1Text = new JTextField();
		UpdateField1Text.setBounds(285, 273, 139, 20);
		contentPane.add(UpdateField1Text);
		UpdateField1Text.setColumns(10);
		
		UpdateValue1Text = new JTextField();
		UpdateValue1Text.setBounds(285, 317, 139, 20);
		contentPane.add(UpdateValue1Text);
		UpdateValue1Text.setColumns(10);
		
		UpdateValue2Text = new JTextField();
		UpdateValue2Text.setBounds(285, 337, 139, 20);
		contentPane.add(UpdateValue2Text);
		UpdateValue2Text.setColumns(10);
		
		JLabel value1Label = new JLabel("Value");
		value1Label.setBounds(150, 220, 46, 14);
		contentPane.add(value1Label);
		
		field1Text = new JTextField();
		field1Text.setBounds(0, 234, 86, 20);
		contentPane.add(field1Text);
		field1Text.setColumns(10);
		
		field2Text = new JTextField();
		field2Text.setBounds(0, 256, 86, 20);
		contentPane.add(field2Text);
		field2Text.setColumns(10);
		
		field3Text = new JTextField();
		field3Text.setBounds(0, 273, 86, 20);
		contentPane.add(field3Text);
		field3Text.setColumns(10);
		
		JLabel field1Label = new JLabel("Field");
		field1Label.setBounds(30, 220, 46, 14);
		contentPane.add(field1Label);
		
		JButton ShowFieldButton = new JButton("ShowField");
		ShowFieldButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Los valores del campo '"+ShowField1Text.getText()+"'");
			    field.put(ShowField1Text.getText(), 1); 
			    DBCursor cursor = coll.find(query, field);
			    while (cursor.hasNext()) {
			        System.out.println(cursor.next());
			    }
			    query.clear();
			    field.clear();
			}
		});
		ShowFieldButton.setBounds(-3, 294, 96, 23);
		contentPane.add(ShowFieldButton);
		
		ShowField1Text = new JTextField();
		ShowField1Text.setBounds(0, 317, 93, 20);
		contentPane.add(ShowField1Text);
		ShowField1Text.setColumns(10);
		
		ShowValueButton = new JButton("ShowValue");
		ShowValueButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Los valores del campo '"+ShowField1Text.getText()+"' con valor '"+ShowValue1Text.getText()+"'");
				query.put(ShowField1Text.getText(), ShowValue1Text.getText());
				DBCursor cursor = coll.find(query);
				while(cursor.hasNext()) {
				        System.out.println(cursor.next());
				}
			    query.clear();
			}
		});
		ShowValueButton.setBounds(102, 294, 108, 23);
		contentPane.add(ShowValueButton);
		
		ShowValue1Text = new JTextField();
		ShowValue1Text.setBounds(103, 317, 107, 20);
		contentPane.add(ShowValue1Text);
		ShowValue1Text.setColumns(10);
		
		ShowField2Text = new JTextField();
		ShowField2Text.setBounds(0, 348, 93, 20);
		contentPane.add(ShowField2Text);
		ShowField2Text.setColumns(10);
		
		ShowValue2Text = new JTextField();
		ShowValue2Text.setBounds(102, 348, 108, 20);
		contentPane.add(ShowValue2Text);
		ShowValue2Text.setColumns(10);
		
		$inButton = new JButton("$in");
		$inButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Los valores "+ShowValue1Text.getText()+" "+ShowValue2Text.getText()+" "+ShowField2Text.getText()+ " del campo " +ShowField1Text.getText());
				    List<String> list = new ArrayList<String>();
				    list.add(ShowValue1Text.getText());
				    list.add(ShowField2Text.getText());
				    list.add(ShowValue2Text.getText());
				    query.put(ShowField1Text.getText(), new BasicDBObject("$in", list));
				    DBCursor cursor = coll.find(query);
				    while(cursor.hasNext()) {
				        System.out.println(cursor.next());
				    }
				    query.clear();			}
		});
		$inButton.setBounds(0, 371, 57, 23);
		contentPane.add($inButton);
		
		$gtButton = new JButton("$gt");
		$gtButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Los valores del campo "+ShowField1Text.getText()+" mayores que "+ShowValue1Text.getText());
				    query.put(ShowField1Text.getText(), new BasicDBObject("$gt", ShowValue1Text.getText()));
				    DBCursor cursor = coll.find(query);
				    while(cursor.hasNext()) {
				        System.out.println(cursor.next());
				    }
				    query.clear();
			}
		});
		$gtButton.setBounds(54, 371, 57, 23);
		contentPane.add($gtButton);
		
		$ltButton = new JButton("$lt");
		$ltButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Los valores del campo "+ShowField1Text.getText()+" menores que "+ShowValue1Text.getText());
			    query.put(ShowField1Text.getText(), new BasicDBObject("$lt", ShowValue1Text.getText()));
			    DBCursor cursor = coll.find(query);
			    while(cursor.hasNext()) {
			        System.out.println(cursor.next());
			    }
			    query.clear();
			}
		});
		$ltButton.setBounds(112, 371, 51, 23);
		contentPane.add($ltButton);
		
		$gt$ltButton = new JButton("$gt$lt");
		$gt$ltButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Los valores del campo "+ShowField1Text.getText()+" mayores que "+ShowValue1Text.getText()+" y menores que "+ShowValue2Text.getText());
			    query.put(ShowField1Text.getText(), new BasicDBObject("$gt", ShowValue1Text.getText()).append("$lt", ShowValue2Text.getText()));
			    DBCursor cursor = coll.find(query);
			    while(cursor.hasNext()) {
			        System.out.println(cursor.next());
			    }
			    query.clear();
			}
		});
		$gt$ltButton.setBounds(54, 396, 108, 23);
		contentPane.add($gt$ltButton);
		
		$neButton = new JButton("$ne");
		$neButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Los valores del campo "+ShowField1Text.getText()+" distintos de " + ShowValue1Text.getText());
			    query.put(ShowField1Text.getText(), new BasicDBObject("$ne", ShowValue1Text.getText()));
			    DBCursor cursor = coll.find(query);
			    while(cursor.hasNext()) {
			        System.out.println(cursor.next());
			    }
			    query.clear();
			}
		});
		$neButton.setBounds(-3, 396, 60, 23);
		contentPane.add($neButton);
		
		$andButton = new JButton("$and");
		$andButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Los campos "+ShowField1Text.getText()+" y "+ShowField2Text.getText()+" con los valores "+ShowValue1Text.getText()+" y "+ShowValue2Text.getText());
			    List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
			    obj.add(new BasicDBObject(ShowField1Text.getText(), ShowValue1Text.getText()));
			    obj.add(new BasicDBObject(ShowField2Text.getText(), ShowValue2Text.getText()));
			    query.put("$and", obj);
			    DBCursor cursor = coll.find(query);
			    while (cursor.hasNext()) {
			        System.out.println(cursor.next());
			    }
			    query.clear();
			}
		});
		$andButton.setBounds(164, 371, 67, 23);
		contentPane.add($andButton);
		
		UpdateField2Text = new JTextField();
		UpdateField2Text.setBounds(285, 295, 139, 20);
		contentPane.add(UpdateField2Text);
		UpdateField2Text.setColumns(10);
		
		AddToDocButton = new JButton("AddToDoc");
		AddToDocButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MongoClient mongoClient3 = new MongoClient("localhost",27017);
				mongoClient3.getDB(ConnectDatabaseText.getText()).getCollection(ShowCollectionText.getText()).update(new BasicDBObject(UpdateField1Text.getText(), UpdateValue1Text.getText()),new BasicDBObject("$set", new BasicDBObject(UpdateField2Text.getText(), UpdateValue2Text.getText())));
				System.out.println("Vas a insertar el campo y valor "+UpdateField2Text.getText()+", "+UpdateValue2Text.getText()+" donde est� el campo y el valor "+UpdateField1Text.getText()+", "+UpdateValue1Text.getText());
			}
		});
		AddToDocButton.setBounds(285, 360, 139, 23);
		contentPane.add(AddToDocButton);
		
		JLabel field2Label = new JLabel("Field");
		field2Label.setBounds(241, 276, 46, 14);
		contentPane.add(field2Label);
		
		JLabel value2Label = new JLabel("Value");
		value2Label.setBounds(241, 320, 46, 14);
		contentPane.add(value2Label);
		
		JButton DeleteFromDocButton = new JButton("DeleteFromDoc");
		DeleteFromDocButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				coll1=database.getCollection(ShowCollectionText.getText());
				coll1.deleteOne(Filters.eq(UpdateField1Text.getText(),UpdateValue1Text.getText()));
			}
		});
		DeleteFromDocButton.setBounds(285, 385, 139, 23);
		contentPane.add(DeleteFromDocButton);
		
		CreateIndexButton = new JButton("CreateIndex");
		CreateIndexButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				coll1= database.getCollection(ShowCollectionText.getText());
			    coll1.createIndex(Indexes.ascending(CreateIndexText.getText()));
			    System.out.println("Has creado el indice correctamente");
			    for (Document index : coll1.listIndexes()) {
			         System.out.println(index.toJson());
			      }
			}
		});
		CreateIndexButton.setBounds(0, 442, 163, 23);
		contentPane.add(CreateIndexButton);
		
		CreateIndexText = new JTextField();
		CreateIndexText.setBounds(0, 476, 163, 20);
		contentPane.add(CreateIndexText);
		CreateIndexText.setColumns(10);
		
		ShowIndexesButton = new JButton("ShowIndexes");
		ShowIndexesButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Estos son los indices de tu collection");
			    for (Document index : coll1.listIndexes()) {
			         System.out.println(index.toJson());
			      }
			}
		});
		ShowIndexesButton.setBounds(173, 442, 114, 23);
		contentPane.add(ShowIndexesButton);
	}
}
